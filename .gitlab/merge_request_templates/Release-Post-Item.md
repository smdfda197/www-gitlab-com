<!-- SET THE RIGHT LABELS AND MILESTONE (let the autocomplete guide you) -->

/label ~"release post" ~"release post item" ~"Technical Writing" ~"devops::" ~"group::"
/milestone %
/assign `@PM`

Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@techwriter` | Engineering Manager: `@EM`

Please review the guidelines for feature block creation at https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks.
They are frequently updated, and everyone should make sure they are aware of the current standards (PM, PMM, EM, and TW).

## Links

- Feature Issue:
- Feature MR (optional):
- Release post (optional):

## Key  dates

**By the 10th:** PMs should draft/submit for review ALL release post item content, whether they are feature or recurring blocks, earlier and no later than the 10th of the month.

**By the 16th:** All required TW reviews as well as any optional PMM and Director/Group PM reviews/revisions should get done between the 11th and the 16th of the month.

**By the 17th:** Release post items need to be marked with the ~"Ready" label by the 17th of the month in order to make it into the current release cycle. 

Note: Drafting release post content well in advance of the 10th is highly recommended, so reviews/revisions can happen in a rolling fashion and not bottleneck against the 17th merge deadline, 

## When to merge

**Reminder: Make sure any feature flags have been enabled or removed!**

Once all content is reviewed and complete, add the ~"Ready" label and assign this issue to the Engineering Manager (EM). The EM is responsible for merging as soon as the implementing issue is deployed to GitLab.com, after which this content will appear on the [GitLab.com Release page](https://about.gitlab.com/releases/gitlab-com/) and can be included in the next release post. All release post items must be merged on or before the 17th of the month. If a feature is not ready by the 17th deadline, the EM should push the release post item to the next milestone.

## PM release post item checklist

**Please only mark a section as completed once you performed all individual checks!**

- Structure:
  - [ ] Screenshot or video is included (required for all changes with a visible UI component).
  - [ ] Check that [image size < 150KB](https://about.gitlab.com/handbook/marketing/blog/release-posts/#images).
  - [ ] Check if the image shadow is applied correctly. Add `image_noshadow: true` when an image already has a shadow.
  - [ ] Remove any remaining instructions (comments).
- Frontmatter:
  - [ ] Check feature availability frontmatter (`available_in:`) is correct: (Core, Starter, Premium, Ultimate). Make sure to set `gitlab_com: false` when the feature isn't available for GitLab.com users.
  - [ ] Ensure the videos added to the frontmatter have the [correct URL format](https://about.gitlab.com/handbook/marketing/blog/release-posts/#videos).
  - [ ] Ensure videos and iframes added within the feature description are wrapped in `<figure class="video_container">` tags (for responsiveness).
  - [ ] Check documentation link points to the latest docs (`documentation_link:`), and includes the anchor to the relevant section on the page if possible. If documentation is not yet available/merged for the feature in question, you may use a placeholder or use the link where the documentation will be added (often the engineer and tech writer know this ahead of time). Be sure to update this placeholder prior to publication if you do not use the final link.
  - [ ] Check that documentation is updated, very clearly talks about the feature (mentions it by the same name consistently in all resources).
  - [ ] Check that all links to `about.gitlab.com` content are relative URLs.
- Content:
  - [ ] Ensure the **why** is clearly explained: what is the problem we are solving for the user, and what value are we delivering?
  - [ ] Make it clear if it is a new feature, or an improvement to an existing feature.
  - [ ] Check title is in sentence case, and feature and product names are in capital case.
  - [ ] Run the content through an automated spelling and grammar check.
  - [ ] Validate all links are functional and have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text).
  - [ ] Ensure [`stage` has the correct format](https://about.gitlab.com/handbook/marketing/blog/release-posts/#stage).
  - [ ] Ensure [`categories` have the correct format](https://about.gitlab.com/handbook/marketing/blog/release-posts/#categories).
- Data:
  - [ ] Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots).
- Review:
  - [ ] Perform your own review.
  - [ ] Directly **assign** this MR to the reviewers.

## Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing, Tech Writing,
and the product director for this area. Please assign them when it is ready to be reviewed!

- [ ] PM assigns this MR to the relevant Tech writer, PMM, and appropriate product or group director for review
- [ ] Tech writer reviewed and approved
- [ ] PMM reviewed and approved
- [ ] Group Manager or Director reviewed and approved
- [ ] PM adds ~"Ready" label and assigns to EM for merge

### Tech writer review

**(required)**

Once **assigned to this merge request**, the [technical writer designated to the corresponding DevOps stage/group](https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments) will perfom their review according to the criteria described below.

**Please only mark a section as completed once you performed all individual checks!**

- [ ] Feature:
  - Confirm if the feature made it into the release milestone. If not, request the PM to confirm and update this MR's milestone accordingly.
  - If the feature has not been merged yet, make sure both feature and docs have been merged:
    - If the feature and the docs are being shipped in the same MR, set this MR dependent on the feature+docs-MR.
    - If the feature is being shipped in an MR apart from the docs-MR, make sure the docs-MR is set dependent on the feature-MR and set this MR dependent on the docs-MR.
  - Ensure the feature is consistently referred by the same term (or by the same feature name) in all resources (docs, release post, `features.yml`, elsewhere).
- [ ] Documentation:
  - Documentation has been updated for the feature. Verify that the feature is clearly described.
    - Note: if you are unsure whether the docs were updated, check the file history looking for a recent update. If you don't find any, check with the PM. If the docs are missing (or unclear, confusing), ask the PM to request the dev who shipped the feature for an MR updating it asap and make sure to review it. If required docs changes are minor, you can choose to do it yourself to speed things up.
  - `documentation_link` links to the correct document and anchor.
  - [Availability](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-availability): product tiers (Core, Starter, Premium, Ultimate, and the corresponding availability in GitLab.com) match in all resources: feature MR, docs ["introduce in GitLab X.Y" note](https://docs.gitlab.com/ee/development/documentation/styleguide.html#text-for-documentation-requiring-version-text), this MR, and `features.yml`.
- [ ] Content:
  - Content accurately describes the feature based on your understanding of it. Look for typos or grammar mistakes. Leave style and messaging for the PM and PMM.
  - All links work and anchors are valid. Do not link to the H1 (top) anchor on a docs page. Links should not redirect. Links to pages within `about.gitlab.com` are given by the relative path, not absolute.
  - Code is wrapped in code blocks.
  - Feature names are capitalized.
  - `documentation_link: 'https://docs.gitlab.com/ee/#amazing'` is wrapped in single quotes and `name: "Lorem ipsum"` wrapped in double quotes.
  - There are no extra whitespaces (end of line spaces, double spaces, extra blank lines, and lines with only spaces).
- Notes:
  - The documentation is part of [GitLab's DoD](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#definition-of-done). A feature is not consider done while there's no documentation for it.
  - If there's something missing from the checklist above, you can request further action for PMs or other team members before approving this MR. You can unassign yourself while there's nothing immediate for you to do, but request to be assigned once the missings tasks are done so you can double-check and approve the MR.
  - Once all your review items have been checked, approve the merge request, check your checkbox in the [review](#review) checklist above, and unassign yourself. Your job is done!

### Director/Group PM review

*(recommended)*

- [ ] Content
  - Ensure the **why** is clearly explained: what is the problem we are solving for the user, and what value are we delivering?

### PMM review

*(recommended)*

**Please only mark this section as completed once you performed all individual checks!**

- [ ] PMM review
  - **problem/solution**: Does this describe the user pain points (problem) as well as how the new feature removes the paint points (solves the problem)?
  - **short/pithy:** Is this communicated clearly with the fewest words possible?
  - **tone clarify:** Is the language and sentence structure clear and grammatically correct?
  - **technical clarity**: Does the descripton of the feature make sense for various audiences, including folks who are not deeply familiar with GitLab?
