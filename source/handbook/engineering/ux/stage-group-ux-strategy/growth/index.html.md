---
layout: handbook-page-toc
title: "Growth UX Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Growth area is made up of Fulfillment, Telemetry and four Growth groups which focus on improving specific metrics. We don't have our own product. Instead, we make the experience of paying for GitLab and managing licenses as easy as can be. We also look for strategies to help customers discover the value of the product, thereby increasing the number of customers and users. GitLab believes that **everyone can contribute**, and this is central to our strategy.

* [Fulfillment Product Direction](https://about.gitlab.com/direction/fulfillment/)
* [Acquisition Product Direction](https://about.gitlab.com/direction/acquisition/)
* [Conversion Product Direction](https://about.gitlab.com/direction/conversion/)
* [Expansion Product Direction](https://about.gitlab.com/direction/expansion/)
* [Retention Product Direction](https://about.gitlab.com/direction/retention/)
* [Growth Engineering Pages](https://about.gitlab.com/handbook/engineering/development/growth/)


## UX team members
The Growth UX team aligns closely to user experience flows rather than with PMs. Designers are not “assigned” to a particular PM, rather they are the first point of contact on UX related to that flow, with flexibility built in to even out the workload and ensure UX experts work on things they are subject matter experts on. This will allow us to cover all the areas of Growth, including fulfillment. It allows designers to own one area but to also have expert knowledge of other areas of Growth's responsibilities.
We also have designated leads for large experience areas, as noted below.

* [Jacki Bauer](/company/team/#jackib) ([Jacki's ReadMe](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md)) - UX Manager 
* [Tim Noah](/company/team/#timnoah) - Senior Product Designer, Renewals, True-ups and Internal Tools (works most on Retention). Lead for Account Management and Internal Tools.
* [Emily Sybrant](/company/team/#esybrant) - Product Designer, Sign-up, Purchase and Account Management (works most on Acquisition)
* [Matej Latin](/company/team/#matejlatin) ([Matej's ReadMe](https://gitlab.com/matejlatin/focus/blob/master/README.md)) - Senior Product Designer, Upgrades and Onboarding (works most on Expansion). Lead for Onboarding. 
* [Kevin Comoli](/company/team/#kcomoli) - Product Designer, Trialing GitLab, Free to paid upgrades and Onboarding (works most on Conversion)
* [Russell Dickenson](/company/team/#rdickenson) - Senior Technical Writer
* [Jeff Crow](/company/team/#jeffcrow) - Senior UX Researcher, Growth

Not sure which designer to talk to about a particular issue? Create your issue and tag it with UX. You can also reach out to the Growth Slack channel (#s_growth) or mention the product designers in issues @gitlab-com/gitlab-ux/growth-ux.

### How We Work
We follow the [Product Designer workflows](handbook/engineering/ux/ux-designer/) and [UX Researcher workflows](https://about.gitlab.com/handbook/engineering/ux/ux-research/) described in the [UX section](handbook/engineering/ux/) of the handbook. In addition:
* we have issue boards so we can see what everyone is up to. 
    * [by assignee](https://gitlab.com/groups/gitlab-org/-/boards/1254597?label_name[]=UX&label_name[]=devops%3A%3Agrowth)
    * [by group](https://gitlab.com/groups/gitlab-org/-/boards/1334665?&label_name[]=UX&label_name[]=devops%3A%3Agrowth)
    * [by workflow](https://gitlab.com/groups/gitlab-org/-/boards/1346572)
* we **label** our issues with UX, devops::growth and group::.
* we use the [workflow labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=workflow%3A%3A) for regular issues and [experiment workflow labels](/handbook/engineering/development/growth/#experiment-workflow-labels) for experiment issues.
* we use **milestones** to aid in planning and prioritizing the four growth groups of Acquisition, Conversion, Expansion and Retention.
    * PMs provide an [ICE score for experiments](https://docs.google.com/spreadsheets/d/1yvLW0qM0FpvcBzvtnyFrH6O5kAlV1TEFn0TB8KM-Y1s/edit#gid=0) and by using [priority labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels) for other issues. 
    * The Product Designer applies the milestone in which they plan to deliver the work (1-2 milestones in advance, or backlog for items that are several months out. For example, if an issue is not doable for a designer in the current milestone, they can add the next milestone to the issue, which will communicate to the PM when the work will be delivered. 
    * If the PM has any concern about the planned milestone, they will discuss trade-offs with the Product Designer and other Growth PMs. 

#### UX Capacity Planning
This is a pilot process we're kicking off in 12.10. The UX team will start weighting UX issues in order to better estimate capacity, realistically break down our work, and give PMs a little insight into how much work we can take on in a milestone. We aim for [velocity over predictibility](https://about.gitlab.com/handbook/engineering/#velocity-over-predictability). This means that we don't need to be accurate, we just need to get better. This paragraph has a [useful explanation of estimation](https://about.gitlab.com/handbook/engineering/development/growth/acquisition-conversion-be-telemetry/#estimation) that captures the spirit of what we want to accomplish with this pilot.

##### UX Weight Definitions

| Weight | Description (UX)                                                                                                                                                                                                                                                                                                                          |
|--------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1      | Mostly small UI changes, smaller UX improvements, without unanswered questions. No UX research, or a quick consult of existing research.                                                                                                                                                                                                                                                  |
| 2      | Simple UI or UX change where we understand all of the requirements, but may need to find solutions to known questions/problems.  No UX research is planned.                                                                                                                                                                                                         |
| 3      | A well-understood change but the scope of work is bigger (lots of UI or UX changes/improvements required). Multiple pages are involved, we're starting to design/redesign small flows. Some unknown questions may arise during the work. This could also be a usability study where the designer is observing sessions but not planning the research.                                                                                                          |
| 5      | A complex change where other team members will need to be involved. Spans across multiple pages, we're working on medium-sized flows. There are significant open questions that need to be answered. There might be a UX research study planned where the designer is planning and conducting some research.                                                                                                                                   |
| 8      | A complex change or new design that spans across large flows and may require input from other designers. This is the largest flow design/redesign that we would take on in a single milestone. This almost definitely requires research where the designer may or may not be working with a researcher. This size issue should be broken up into smaller issues.                                                                                                                                                         |
| 13     | A significant change that spans across multiple flows and that would require significant input from others (teams, team members, user feedback) and there are many unknown unknowns. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller issues. |


##### Process
*  First, break the issue down into the UX parts so that we can add a weight without it throwing off the engineering team's weights. The product designer should create their child issues based on their understanding of the work. 
    *  A parent issue will use label `link::parent` and contain high level discussion and links to child issues
    *  Child issues will use label `link::child` and we can create these for different parts of the product development flow. So for example, you can have a child issue for the UX work (validation and design), or you can break these down even further.
*  Label UX issues with `UX` and engineering issues with `Engineering`. This will make sure there is no confusion as to what the weight is for.
*  The UX team will estimate the capacity of a designer to work on issues as part of the milestone, vs their other responsibilities such as visual reviews and Pajamas work.
*  For the first milestone we will meet as a team and use the definitions to apply weights to issues as best we can. At first we will be guessing a bit and that's OK. When we get better at this, we can do it async.
    *  A weight of 8 or 13 indicates that we should break down the issue further. We can break an issue down into a problem validation issue, a user workflow issue, a design issue, and/or a solution validation issue, for example. Or, we could break up the work into smaller pieces as in a part of a feature, or a part of a workflow. There's no one right way to do this, so let's discuss and learn as we go.  
*  If a new issue is added during the milestone, we will need to add a weight to it. 
*  After the milestone, we will compare what we planned with what we actually delivered and the number of person days we had to get the work done. Then we will use that as a baseline for the next milestone. 



## Themes and Focus Areas
The UX Themes that we are prioritizing for now (FY21 Q1):

* [Easy to Understand Pricing](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Easy%20to%20Understand%20Pricing)
* [Effortless Purchase Flow](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Effortless%20Purchase%20Flow) (.com and SH)
* [Transparent Billing and Renewal](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Transparent%20Billing%20and%20Renewal)
* [Feature Discoverability](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Feature%20Discoverability)
* [Easy Onboarding for Quicker Productivity](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Easy%20Onboarding)
* [Implementing Pajamas](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Implement%20Pajamas) (upgrade customer portal to use Design System): [Epic](https://gitlab.com/groups/gitlab-org/-/epics/1886)


Other important themes that we aren't prioritizing for now but will look at in the future and/or in small .
* [Eliminate Multiple Logins](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Eliminate%20Multiple%20Logins)
* [Internal Tools for Team Members](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Internal%20Tools)
* [Effortless Upgrade Flow](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Effortless%20Upgrades)
* [Effortless Installation](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Effortless%20Installation)
* [Effortless Trials](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=UX%3A%20Effortless%20Trials)

Applying theme based labels to UX issues allow us to track our work more holistically against big areas we've identified for UX improvement.

### Customer Journey/Process for Fulfillment/Customer Transactions

When working through transactional issues related to sign-up, trials and upgrades it helps to break down the task into pieces. This way of working through issues enables product designers to document the beginning and end of a user journey in an easily digestible way for everyone. It's based very loosely on a talk from Jared Spool regarding "Content and Design".
* Entry Point(s): The initial touch points of user interactions. (i.e. A Page, CTA or Form etc)
* Decision: Giving users the ability to decide on Products, Options or Packages.
* Confirmation: Summary of a successful or unsuccessful purchase.

These steps won't always be needed and won't always be linear. For instance, an Entry Point may also be a point at which a user selects a Product. 


## UX Scorecards
All of the planned, in progress and completed UX Scorecards for Growth can be found in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/2015).
For more information, read about [UX Scorecards](https://about.gitlab.com/handbook/engineering/ux/ux-scorecards/).

##### Scorecard: Renew a GitLab Plan
* Job Description: *TBD: When (situation), I want to (motivation), so I can (expected outcome).*
* UX scorecard issue (with walktrhough and video): [113](https://gitlab.com/gitlab-org/growth/product/issues/114) (WIP)
* UX scorecard Score: TBD
* Recommendations Epic: TBD

##### Scorecard: Start a GitLab trial
* Job Description: *When creating a new account, I want to start a trial, So I can test GitLab.com Gold features.*
* UX scorecard Epic (with walkthrough and video): [1332](https://gitlab.com/groups/gitlab-org/-/epics/1332)
* UX scorecard Issue: [1355](https://gitlab.com/groups/gitlab-org/-/epics/1355)
* UX scorecard Score: D- (Q2, 2019) 
* Recommendations: [1356](https://gitlab.com/groups/gitlab-org/-/epics/1356)


##### Scorecard: User onboarding

* Job description: *When I sign up for a GitLab account, I want to see what are its main features and benefits for my role/team, so I can find out if it’s potentially valuable to our company.*
* UX scorecard issue (with walkthrough and video): [170](https://gitlab.com/gitlab-org/growth/product/issues/170)
* UX scorecard Score: C- (Q4, 2019)
* [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/4)

##### Scorecard: Upgrade a GitLab Plan

* Job Description: *When I realise my team needs a feature from a higher tier than our current paid one, I want to quickly and easily upgrade (to that tier), so that they can benefit from it as soon as possible.*
* UX scorecard issue (with walktrhough and video): [113](https://gitlab.com/gitlab-org/growth/product/issues/113)
* UX scorecard Score: C- (Q4, 2019)
* [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/22)

##### Scorecard: Buy add-on CI minutes

* Job Description: *When I realise that we're running out of CI minutes, I want to quickly and easily  buy more, so that our team can continue building and delivering software uninterrupted.*
* UX scorecard issue (with walktrhough and video): [531](https://gitlab.com/gitlab-org/gitlab-design/issues/531)
* UX scorecard Score: C (Q4, 2019)
* [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/26)

##### Scorecard: Upgrade a GitLab.com subscription tier

* Job Description: *When I realise my team needs a feature from a higher plan than our current paid one, I want to quickly and easily upgrade (to that plan), so that they can benefit from it as soon as possible.*
* UX scorecard issue (with walktrhough and video): [947](https://gitlab.com/gitlab-org/gitlab-design/-/issues/947)
* UX scorecard Score: C (Q1, 2020)
* [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/32)


## Log of major changes

This is a log of major changes introduced by the Growth UX team as part of their work with the [Growth subgroups](/handbook/engineering/development/growth/). It serves as an easy way to track down when and why a major change to a user experience was introduced. 

We define "major changes" as:

* Major layout/IA changes on a single screen
* User experience changes across multiple screens (flow changes)
* Increasing/decreasing the number of interactions it takes to complete a task
* Changing the content of something at a critical step for the users (a banner warning for example)
* Changing when and where something that is triggered by the system happens (when and where a banner shows up and what triggers it)

### Introduced the new, simpler free trial signup flow

Feb 18, 2020, [Epic](https://gitlab.com/groups/gitlab-org/-/epics/377) - *Released in 12.4*

Last year we introduced a simpler free trial sign up in which a user could complete the process by interacting with one app only. Before, they had to create a separate account in the Customer Portal app which often led to confusion. 

### Provide more context and guidance for true-ups in the renewal flow

Jan 30, 2020 - [Issue](https://gitlab.com/gitlab-org/growth/engineering/issues/40) - *MVC expected in 12.8*

Users didn’t know what number to put into the *Users over license* field in the renewal flow which resulted in new licenses that threw errors. They also didn’t know what number to put into the *Users* field so we renamed it so it aligns with the data and labels in the Admin Overview. The MVC will be shipped without the illustrations but is still considered a major improvement. This change as a whole is an intermediate step before we move towards automatically collecting the data.

### Changed the appearance, content and behavior of renewal and auto-renewal banners

Jan 30, 2020 - [Issue 1](https://gitlab.com/gitlab-org/growth/product/issues/102), [Issue 2](https://gitlab.com/gitlab-org/growth/product/issues/143) - *Expected to be delivered in 12.8*

Existing banners were confusing the users because they lacked contextual information. Auto-renewal banners, for example, didn’t make it clear that the subscription will automatically renew. Banners were also non-dismissable and shown to all users instead of just the instance admins. This change introduced a new appearance, new behaviour (who they’re shown to and when) and more contextual content.

<!-- Template
### Short, descriptive title for the change

Timestamp (MMM DD, YYYY) - [Issue](link_to_issue) - *When was it delivered (milestone)*

Short description of the change and why it was made. 
-->
