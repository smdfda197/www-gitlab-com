---
layout: handbook-page-toc
title: "Commercial Sales Enablement"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Commercial Sales Enablement Handbook Page

### Opp Management - Managers check this

**Fields that need to be updated at all times**
1. Close date
2. $ value (by a t-shirt size)
3. Opportunity name (needs to have tier and user count)
4. Account tier and tier notes
5. Primary contact
6. Next steps
7. Next Steps Date
8. Stage
9. Any activity that’s logged while working the opportunity needs to be attached to the contact and the opportunity
   * Best Practice is to log an activity in ‘activity history’
10. Command Plan

**This is what you’re going to learn in this course**
1. Here are all the reasons why ppl say they can’t fill in the Command Plan
   * I haven’t talked to them yet
   * They didn’t tell me
   * I don’t know anything about the account/company
   * I’m going to learn that on the next call
2. The above are **not** acceptable reasons not to fill in the required fields listed above.

**How to get each of those required fields**
1. Research online
2. Write the discussion questions you’re going to ask to obtain the info
3. Qualification Indicators (101)
   * How to get these - Qual 201
4. Other SFDC field requirements
5. Recap on the objections
   * Review the list and reiterate that they’re not acceptable reasons to not fill out those fields


### Tiering Accounts

* [Commercial Sales: Tiering Accounts Training Clippet](https://youtu.be/M-5OhlYxmFI)

For more details visit the [Account Tiering](/handbook/sales/commercial/#account-tiering) section on the Commercial Sales Handbook page.


### Getting into Accounts That Say They Don't Have a Problem

**In Concept**
1. Each of us sales professionals makes a regular decision: where we apply our time. We are trying to optimize the ratio of output to time. Everything we do or work on should be judged by this ratio.
2. Each of us has a market to go after: note that most of our volumes of contacts to connect with is roughly similar - you either have:
   * Lots of accounts with less contacts
   * Or few accounts with many, many more contacts
3. We in MM and SMB use the Account Tiering fields to effectively show ourselves where we think there is a higher or lower level of this ratio.

**Each person writes out a process:**
1. First: come up with a t-shirt size for each account overall, and how far on the journey they are
2. Find all easily attainable contacts and put in SFDC
3. Prioritize in order of most likely to speak/reply
   * Bucket into 3 groups: know nothing, know good info, and not sure value of what i know
4. Plan the method and type of content for reach out for each group/bucket
5. ID your ideal client: personally I want some form of intro…
   * A slow valuable intro is better than most fast, cold connects
6. Get a 10-15 min connect in whatever way they prefer:
   * Nice to meet you - how’s it going?
   * Give your credibility statement.
   * Ask them if they think there is anyway that GitLab could help.
   * When they say no, be sad. Then ask what challenges their team or they have.
   * When they say yes, listen up and begin discovery, then bounce back to qualification to understand if its worth it for them to work on it.
7. Document all notes and immediately build the most compelling customized deck on why they should try Gitlab
   * See where the biggest gaps are in the deck, and if you aren’t sure, role play with a colleague on it to see what is missing
8. ID EXACTLY the info missing to make it somewhat compelling
9. Go back to your list of contacts and find out who could best get you this info

**The tougher, non-intro’d scenario:**
1. Dig in on the persona of the contact info you have so you can reach out in a way that you have 100% confidence that this connect will result in 2-way communication
   * This can be showing up at an event
   * It can be somewhat person based
   * It should never be creepy
   * Think about how to give first
2. When coming up with a strategy, think about the specific reason that a person inside that company has NOT offered their contact into to GitLab, and figure out how to get over that single hump

**Make sure that you document all:**
1. Notes
2. Ideas
3. Internal strategy

Be sure to put in the status of this account in Account Tiering and Tiering Notes so that you can be efficient every time you or another person works on it

**When is it Game Over?**
1. It is game over when working on an account reveals information that moves it below another account you are working that surpasses it in terms of value.
2. Value = Worth (what you will get)/Cost (what you need to put in) x Risk (the chances you predict that your understanding of value and worth are accurate.
3. Value (in this specific effort) = how much the account is worth this FY/the time you need to get them to spend that amount

