---
layout: markdown_page
title: "Use case: Source Code Management"
---



## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Source Code Management

As organizations accelerate delivery, through DevOps, controlling and managing different versions of the application assets from code to configuration and from design to deployment is incredibly important.
Velocity without robust version control and traceability is like driving a car with out a seatbelt.

Source code management is more than simply tracking changes, versions, and branches of code. Effectively, it includes practices such as:

- Enabling development teams to work in distributed and asynchronously
- Managing changes and versions of code and artifacts
- Enabling *Review* and *Collaboration* of code and other assets
- Tracking approvals of proposed changes
- Resolving merge conflicts and related anomalies

In general, Source Code Management is required because software is constantly changing. Regardless of the stage of development, there will be change to deal with.

> No matter where we are in the system life cycle, the system will change, and the desire to change it will persist throughout the life cycle.

> E.H. Bersoff, 1980.

Changes are of different nature, here a short list:

| Change | Outcome |
| --- | --- |
| Requirements or features may change in scope | Business changes |
| New team members may join | Stakeholder changes |
| Docs are updated all the time | Technical changes |
| Dependencies may have changed | Technical changes |
| The actual code will change!| Technical changes |

The multiplicity of changes involve a myriad of users and stakeholders which bring to the picture their motivations and initiatives to the usage of source code management. The next exercise tries to condense them into user and buyer personas.

## Personas

### User Persona
Being the entry point to GitLab means that many user personas find utility and a solution to their problem in Source Code Management. Let's go through the list of power user personas and describe briefly their key motivations to use Source Code Management in GitLab:

[Parker the Product Manager](/handbook/marketing/product-marketing/getting-started/102/#step-three-get-docker-and-configure-middleman)
- PMs coordinate feature development and project success among other things. The ability to monitor progress through commits, review app and validate those changes and provide feedback is key to they success of their role
- These changes, in time will generate valuable statistical insight for the PM to assess accurately development efforts of planned new features

[Delaney the Development Team Lead](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/38512/commits?commit_id=b0887cc5146ba8c31c0422c06094efb7b2975f60)
- Just like the PM, Team Leads need also to understand their team's capacity to assign upcoming tasks to meet goals on time accordingly
- Approval workflows in Code Review allows them to become faster real team work enablers

[Sasha the Software Developer](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/38512/commits?commit_id=b0887cc5146ba8c31c0422c06094efb7b2975f60)
- Sasha takes advantage of both Command Line Tools and GitLab's GUI to have complete control of every commit he does to complete his tasks
- Even when scope changes, a frequent hurdle and a source of frustration, branching, merging and conflict resolution will be performed in Source Code Management and will trigger CI for fast resolution

[Devon the DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
- All information relevant to the role's goals is congregated in Source Code Management to take action on it. Time to resolution and, in general, any other key metric in DevOps is measured and its performance tracked in Source Code Management
- Any improvement applied to the development process will reflect in Source Code Management's interfaces, whether its the Merge Requests or Issues.

### Buyer Personas
Source Code Management purchasing typically do not require executive involvement. It is usually acquired and installed via our freemium offering without procurement or IT's approval. This process is commonly known as shadow IT and its a great opportunity for us to eventually become a paid for service. When the upgrade is required the [VP of IT](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/) is the most frequent decision maker. The influence of the [VP Application Development](/handbook/marketing/product-marketing/roles-personas/buyer-persona/#alex---the-application-development-manager) is notable too to the owner of the budget.

## [Message House](./message-house/)

The message house for the use case provides a structure to describe and discuss the value and differentiators for the use case.

## Industry Analyst Resources

Research relevant to this use case can be found in the [Analyst Reports - Use Cases](https://docs.google.com/spreadsheets/d/1vXpniM08Ql0v0yDd22pcNmXpDrA-NInJOwj25PRuHXA/edit?usp=sharing) spreadsheet.

The AR Handbook page has been updated to reflect [how we plan to incorporate Use Cases into our analyst conversations](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/#how-we-incorporate-use-cases-into-our-analyst-conversations).

For a list of analysts with a current understanding of GitLab's capabilities for this use case, please reach out to Analyst Relations via Slack (#analyst-relations) or by submitting an [issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new) and selecting the "AR-Analyst-Validation" template.




## Market Capabilities

| Market Capability | Description | Typical features that enable this capability | Value / ROI |
|---|---|---|---|
| **Protect and secure product assets** | The solution provides mechanisms to manage, track, and maintain access to and control of changes to product development assets | Single sign-on, code ownership, change reviews, change approvals, IP whitelisting, Activity stream, GPG signed commit, Reject unsigned commits, Protected branches, branching, committer access rules, etc. | Secures IP and project access  |
| **Enterprise Ready** | The solution is able to support large scale, widely distributed teams with consistent practices, processes, and controls  | High Availability, fast and responsive application, project templates, file templates, access controls, auditability, and traceability.  | Prevents outages and disruptions of development team work. Enables traceability to authors of changes to address defects or bugs in the product and auditability throughout |
| **Supports numerous project types** | The solution is able to manage and maintain the version history of the various assets and support the development patterns of many different types of project types | Component reuse, traceability, design management, branching, diffing, merging, object storage, design versioning | Able to manage assets and files for the entire development team, no matter how diverse, creating a single source of truth for the product configuration and making visibility and communication available at every level |
| **Foster Collaboration** | The solution enables teams to efficiently collaborate on proposed changes, reviewing work, and improving the quality of their changes  | Create new branches of the project, add new files/assets, collaborate on proposed changes, review comments, suggest changes, webIDE | Code quality increase and improved release velocity through team review and validation|
| **Always available** | The solution provides high performance for all compute, networking and store activities | Git protocol v2 support, Deduplicate Git objects for forked repositories, HA, Download single repository files, | User satisfaction at the interaction level can be maximized with a tool that is always available and performs strongly |
| **Workflow Automation** | The solution streamlines team workflow by connecting and initiating repetitive activities to enable the team to remain focused on their work and productivity | Automatically update or close related issue(s) when a merge request is merged, Configurable issue closing pattern, display merge request status for builds in CI system, visibility into security scans and build stats.  | Reduce repetitive tasks and work, enabling the development team to become more productive and efficient. |
| **System Design and Architecture Browsing** | The solution should help design, manage and navigate the most common architectural patterns: from large monorepos (with components) to service-oriented architectures (microservices being an example of it) and provide | Git Submodules, Comprehensive and easy to use web interface, webIDE | By providing browsing of the structure of each component and the codebase as a whole, SCM solutions make concurrent work easier, onboarding faster and help define goals in the shape of deliverable units.


## Top 3 Differentiators

| Differentiator | Value | Proof Point  |
|-----------------|-------------|---------------|
| **Distributed version control** | It allows for asynch, remote, collaborative work to flourish since a single copy of the complete project's history can be stored in any machine, branching is easy and powerful so almost endless workflow possibilities open in opposition to centralized VCS like Perforce or CVS. All the information different teams produce while collaborating on source code and other digital assets in GitLab can be easily analyzed, authorized and streamlined from the Merge Request with clockwork precision. This, in turn, allows for team leads to correctly implement best practice workflows like [GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html) | –  Stackoverflow's 2018 survey says [87% of respondents use Git](https://insights.stackoverflow.com/survey/2018#work-_-version-control) (jump from 69% in 2015) as opposed to other centralized and distributed VCSs. Similar trend is captures in the [Open Hub data](https://softwareengineering.stackexchange.com/questions/136079/are-there-any-statistics-that-show-the-popularity-of-git-versus-svn/136207#136207). In 2019 they [didn't even ask the question](https://insights.stackoverflow.com/survey/2019#development-practices) in the same survey.  –  [Gartner's Market Guide for Software Change and Configuration Management](https://www.gartner.com/en/documents/3118917/market-guide-for-software-change-and-configuration-manag) from 2015 lays out clearly the advantages of DVCS. In 2019 Gartner assess SCM as part of Application Release Orchestration of which [GitLab is a challenger as of 2019](/analysts/gartner-aro19/) – [Google trends since 2004](https://trends.google.com/trends/explore?date=all&q=git,svn,perforce,mercurial,tfs) compared to other DVCS and CVCSs.
| **Single Application** | The ability to connect every phase of the Software Development Lifecycle in one single DevOps platform. One data layer, one design system, one set of commands to manage all the different stages of software design, development, build and deployment | [General proof points](/handbook/sales/command-of-the-message/proof-points.html) of the single app  |
| **Product Development Management** | GitLab is the only product that increasingly provides collaboration functionality to Product teams that work not only with source code but also and IP, graphic assets, animations and binaries to mention a few.  | Forrester's [Adopt Product Management to Connect Design and Development](https://www.forrester.com/report/Adopt+Product+Management+To+Connect+Design+And+Development/-/E-RES149995) clearly states that "Siloed Design And Dev Teams Deliver Subpar Software" |

---


## Competitive Comparison
TBD

## Proof Points - Customer Recognitions

[General proof points](/handbook/sales/command-of-the-message/proof-points.html) and [Customer Recognition](/customers/marketplace/)

### Quotes and reviews

#### Gartner Peer Insights

*Gartner Peer Insights reviews constitute the subjective opinions of individual end users based on their own experiences and do not represent the views of Gartner or its affiliates. Obvious typos have been amended.*

>"The software is intuitive and quite easy to use. Since many software development projects require more than one person, this makes it easy to create teams and collaborate."
>
> - **Quality Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1037713)
>
>"Improves productivity of engineers by providing easy and fast ways to keep feature branches and merge them quickly and efficiently."
>
> - **Engineering Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1060524)
>
>"Keeps your software projects under control. Rogue developers are kept at bay via enforced review processes and pipelines."
>
> - **Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1063180)
>
>"For managing git repositories it is the best product available right now in the market."
>
> - **Sr. Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1074452)
>
>"This has really aided in our ability to automate software delivery and return wasted overhead back to the pool of resources! This is a very simple to use and fast delivery tool to assist your code pipeline."
>
> - **Project Manager**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1078302)
>
>"We use this platform in our company to version our source [code] ensure they are up to date and as a backup option. It enables us build scalable and high quality products. Ease of use and compatible with most development environments."
>
> - **Sr. Software Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1135664)
>
>"I appreciate its ability to run limitless. It has various features like issue tracker, protected branches and merge requests, which gives very nice experience."
>
> - **Sr. Software Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1142879)
>
>"Gitlab is a very useful SCM. In our [organization] we have used it as a source code repository. We have extensively used branching and tags creation feature. As we work in a sprints, we have several sprint and feature branches."
>
> - **Lead Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1144638)
>
>"GitLab is a superb source code management [provider]."
>
> - **Systems Engineer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/application-release-orchestration-solutions/vendor/gitlab/product/gitlab/review/view/1194415)
>
>"Before GitLab, we used to make local copies of code or backup the code and then pass on the code through the server. But if our organization knew about GitLab from start, we would have immediately integrated with our development practises for ease of deployment."
>
> - **Software Developer**, [Gartner Peer Review](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendor/gitlab/product/gitlab/review/view/1016152)

### Case Studies

* At [ESA (European Space Agency)](/customers/european-space-agency/) GitLab is used as a central version control system to allow opportunities for collaboration, synergies and multiple exploitations of efforts in visible way.

GitLab was validated and adopted for the European Space Agency as a code repository platform in 2016. Usage was initially limited to a hand-picked group of first-wave users, but demand quickly escalated. In just two years, more than 140 groups adopted GitLab as their software versioning tool. Across ESA, more than 1500 software projects have been created. These range from mission control systems, onboard software for spacecraft, image processing and monitoring tools for Labs. The ESA IT Department also uses GitLab to host their code tools and configurations infrastructure..

* All the new strategic pieces of [Goldman Sachs](/customers/goldman-sachs/)’ software development platforms are tied into GitLab. GitLab is used as a complete ecosystem for development, source code control and reviews, builds, testing, QA, and production deployments.

We’re bringing into the firm a platform that our engineers actually want to use – which helps drive adoption across multiple teams and increase productivity without having to ‘force’ anyone to adopt it. This is really helping to create an ecosystem where our end users are actively helping us drive towards our strategic goals - more releases, better controls, better software..
> George Grant, VP technology fellow, Goldman sachs

* [Worldline](/customers/worldline/) benefits from Git availability through GitLab. With their precious installation of Subversion, Worldline faced bottlenecks and lack of ownership. It took 1-2 weeks to get a source code repo, now it takes a few seconds.
> We started using GitLab because we wanted to get an easy Git repository management system and because we wanted people to be able to use merge requests. We wanted the ability to have more code reviews and to ease discussions between developers.
>
> Antoine Neveux, Software Engineer, Kazan Team, Worldline.

The adoption of GitLab was quite successful and, within six months, over 1,000 users were active users. Developers explained that the adoption rate was high because GitLab is so easy to use. People actually felt encouraged to contribute code reviews with GitLab Merge Requests. Previous code review tools had 10-20 developers using them, while Worldline currently has 3,000 active users of GitLab - an adoption rate increase of 12,000 percent.

* [Commit San Francisco 2020: Why we chose GitLab as our Enterprise SCM](https://youtu.be/kPNMyxKRRoM) by Northwestern Mutual. [Deck](https://docs.google.com/presentation/d/1vxiFJShRjU98kvwK87PZnYnMzBVPT5VJTr1chUEiM_Y/edit#slide=id.g6d6088e69c_0_15)

NWM had several challenges: their code base was fragmented. Dev permissions were also complex to handle. In part this hold off devs from collaborating which is critical fro code quality, deploying faster and fixing bux and security holes. This was all consequence of a vast and disperse toolchain. They decided to go for GitLab for current and future superiority of features given release velocity and community driven open source project. Upcoming security features were highly promising and deliver could be tracked in real time since GitLab operates in the open. A full migration to their Enterprise environment was completed 8 months. After implementation they managed to reduce friction with the ease of use of GitLab's CI. Collaboration sparked in the WebIDE.

### References to help you close
[Links to Salesforce References](https://gitlab.my.salesforce.com/a6l4M000000kDwu) Note: Sales team members should have access to this report. If you do not have access, reach out to the [customer reference team](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#which-customer-reference-team-member-should-i-contact) for assistance.

## Key Value (at tiers)

### Premium/Silver
- Describe the value proposition of why Premium/Silver for this Use Case

### Ultimate/Gold
- Describe the value proposition of why Ultimate/Gold for this Use Case

## Resources
### Presentations
* WIP

### Source Code Management Videos
* [Source Code Walk Through, January 2020](https://www.youtube.com/watch?v=wTQ3aXJswtM) by James Ramsey, Group Product Manager for the Create Stage.
* [GitLab Flow pattern](https://youtu.be/InKNIvky2KE?list=WL)
* [Design Management Walkthrough](https://youtu.be/LzFRBMGl2SA) by Christen Dybenko, PM Knowledge Group.
* [Web IDE walkthrough](https://youtu.be/6VI_SqkcQIQ?t=366) by William Chia sPMM CI/CD.
* [Merge Request and Source Control as part of the Software Development Life Cycle](https://youtu.be/UuX-GnYWNwo?t=274) by William Chia sPMM CI/CD.
* [User pain points and improved code reviews](https://www.youtube.com/watch?v=utfq9-ExXGE) by Pedro Moreira da Silva, UX Manager for the Source Code Group.

### Integrations Demo Videos
* [Jira & Jenkins Integration Video](https://www.youtube.com/embed/Jn-_fyra7xQ)
* [How to setup the Jira Integration](https://www.youtube.com/watch?v=p56zrZtrhQE)
* [GitHub Integration Video](https://www.youtube.com/embed/qgl3F2j-1cI)

### Clickthrough & Live Demos
* [All Marketing Click Through Demos](/handbook/marketing/product-marketing/demo/#click-throughs)
* [All Marketing Live Demos](/handbook/marketing/product-marketing/demo/#live-instructions)
